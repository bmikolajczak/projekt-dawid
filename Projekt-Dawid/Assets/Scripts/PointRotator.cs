﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointRotator : MonoBehaviour
{
    private Vector3 _rotation;
    private void Start()
    {
        _rotation = new Vector3(60f, 20f, 40f);
    }
    void Update()
    {
        //vector3 do zmiennej
        transform.Rotate(_rotation * Time.deltaTime);
    }
}
