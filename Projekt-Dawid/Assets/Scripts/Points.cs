﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    public Text countText;
   
    public void OnEnable()
    {
        PlayerMovement.onPoints += UpdatePoints;
    }
    public void UpdatePoints(int points)
    {
        countText.text = "Score: " + points.ToString();                
    }

}
