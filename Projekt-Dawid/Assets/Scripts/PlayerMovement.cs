﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour
{
    private Rigidbody _rb;
    public float normalSpeed;
    public float maxSpeed;
    private float _speedBonus;
    private bool _onTheGround;
    public delegate void OnPointsGet(int currentPoints);
    public static event OnPointsGet onPoints;
    public delegate void OnButtonPressed();
    public static event OnButtonPressed onButton;
    public int Points { get; set; }
    public Text finishText;
    private Vector3 _jump;
    private Vector3 _movement;
    private Vector3 _position;
    private string _speedup = "speed-up";
    private string _jumpster = "jumpster";
    private string _button = "button";
    private string _floor = "floor";
    private float _moveHori;
    private float _moveVert;

    //zwykły ruch do przodu, kamera z offsetem na sztywno, żadnego obracania.


    void Start()
    {
        Points = 0;
        normalSpeed = 10f;
        maxSpeed = 46f;
        _speedBonus = 34f;
        _rb = GetComponent<Rigidbody>();
        finishText.GetComponent<Text>().enabled = false;
        _position = new Vector3(46.38f, 34.91f, -773.2f);
        _jump = new Vector3(0f, 5f, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        //Game Over Text
        //nie po tagach 
        //if (other.gameObject.GetComponent<>())<-chcieliśmy tego użyć ale nasze przyciski nie mają nic specjalnego i gdy otwieramy drzwi to wyświetla sie napis "Game over" więc taki są nam potrzebne
        if (other.gameObject.CompareTag("finish"))
        
        {
            finishText.GetComponent<Text>().enabled = true;
            finishText.text = "Game Over";
        }
        

        if (other.gameObject.CompareTag("point"))
        {
            other.gameObject.SetActive(false);
            if (onPoints != null)
            {
                Points = Points + 1;
                onPoints(Points);
            }
        }
        if (other.gameObject.CompareTag("teleport"))
        {    
            //vector3 na sztywno ustawcie na starcie 
            transform.position = _position;
        }
        //Activable Objects
        //stringi do zmiennych
        if (other.gameObject.CompareTag(_speedup))
            normalSpeed = normalSpeed + _speedBonus;
        if (other.gameObject.CompareTag(_floor))
            _onTheGround = true;
        if (other.gameObject.CompareTag(_jumpster))
            _rb.AddForce(new Vector3(0f, 14f, 0), ForceMode.Impulse);
        if (other.gameObject.CompareTag(_button))
        {
            if (onButton != null)
                onButton();
        }

    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("speed-up"))
        {
            normalSpeed = normalSpeed - _speedBonus;
        }
       
    }
    

    void FixedUpdate()
    {
        //player movement
        //wywalic deklaracje nad start 
         _moveHori = Input.GetAxis("Horizontal");
         _moveVert = Input.GetAxis("Vertical");
         _movement = new Vector3(_moveHori, 0f, _moveVert);
       _rb.AddForce(_movement * normalSpeed);

        if (Input.GetKey(KeyCode.Space) && _onTheGround == true)
        {
            //vector3 do zmiennej
            _rb.AddForce(_jump, ForceMode.Impulse);
            _onTheGround = false;
        }
        //speeding limit
        //getcomponent do awake i używać zmiennej <- This is not how speed limit works, wywala bład
        if (_rb.velocity.magnitude > maxSpeed)
        {
            _rb.velocity = _rb.velocity.normalized * maxSpeed;
        }

    }
}
