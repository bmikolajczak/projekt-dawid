﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class Doors : MonoBehaviour
{
    
    private float _speed = 2f;
    public Transform door;
    public Transform door2;
    // Start is called before the first frame update
    
    public void OnEnable()
    {
        PlayerMovement.onButton += OpenGates;
    }
    public void OpenGates()
    {
        StartCoroutine(Door());        
    }

    IEnumerator Door()
    {
        while(TransformUtils.GetInspectorRotation(door).y>-45)
        {
            door.Rotate(new Vector3(0f, -_speed / 10, 0f));
            door2.Rotate(new Vector3(0f, _speed / 10, 0f));
            yield return new WaitForEndOfFrame();
        }

    }
    

}
